// Выведите элементы массива в виде таблицы table так, 
// чтобы каждое поле объекта попало в свой тег td. 
// Сделайте заголовки колонок вашей таблицы.


import { v4 as uuidv4 } from 'uuid';

const TableCreation = () => {
  const users = [
    {id: uuidv4(), name: 'user1', surn: 'surn1', age: 30},
    {id: uuidv4(), name: 'user2', surn: 'surn2', age: 31},
    {id: uuidv4(), name: 'user3', surn: 'surn3', age: 32},
  ];
  const userTableHead = (
    <thead>
      <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Surname</th>
        <th>Age</th>
      </tr>
    </thead>)
  const usersTable = (
    <>
      {userTableHead}
      <tbody>{users.map((el,index) => (
        <tr key={el.id}>
          <td>{el.id}</td>
          <td>{el.name}</td>
          <td>{el.surn}</td>
          <td>{el.age}</td>
        </tr>
      ))}
      </tbody>
    </>
  )
  return (
    <>
      <table>{usersTable}</table>
    </>
  )
}


export default TableCreation;