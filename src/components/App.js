import './App.css';
import BirthYear from './Birth-year';
import CheckboxRadioSelect from './Checkbox-radio-select';
import CountSymbols from './Count-symbols';
import SimpleCalc from './Simple-calc';
import StatesIntro from './States-intro';
import TableCreation from './Table-creation';
import Todo from './Structures/Simple-todo';


function App() {
  return (
    <div className="App">
      <header className="App-header">
        {/* <TableCreation /> */}
        {/* <StatesIntro />  */}
        {/* <BirthYear/> */}
        {/* <SimpleCalc/> */}
        {/* <CountSymbols/>  */}
        {/* <CheckboxRadioSelect/> */}
        {/* <Structure /> */}
        <Todo/>
      </header>
    </div>
  );
}

export default App;
