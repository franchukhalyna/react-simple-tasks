// Пусть вы хотите отображать на экране данные юзера: его имя, фамилию, возраст. Сделайте для этого соответствующие стейты с начальными значениями.
// Сделайте кнопки для изменения имени и фамилии.
// Добавьте еще один стейт, который будет показывать, забанен пользователь или нет. Выведите информацию об этом в каком-нибудь теге.
// Сделайте кнопку, нажатие на которую будет банить пользователя и кнопку, нажатие на которую будет разбанивать пользователя.
// Модифицируйте предыдущую задачу так, чтобы из двух кнопок всегда была видна только соответствующая. То есть, если пользователь забанен, то видна кнопка для разбанивания, а если не забанен - для забанивания.
// Сделайте еще две кнопки. Пусть первая кнопка увеличивает возраст на единицу, а вторая - уменьшает его.
import { useState } from "react";


const StatesIntro = () => {
  const [name, setName] = useState('None');
  const [surname, setSurname] = useState('None');
  const [userBan, setUserBan] = useState(true);
  const [age, setAge] = useState(30);
  return (
    <div className="states-intro">
      <p>Your name is {name}</p>
      <p>Your surname is: {surname}</p>
      <button onClick={() => setName('Galina')}>Change name</button>
      <button onClick={() => setSurname('Fr')}>Change surname</button>
      <p>User: {userBan && 'you banned'}</p>
      <button onClick={() => userBan ? setUserBan(false) : setUserBan(true)}>{userBan ? 'Unban' : 'Ban'}</button>
      <p>{age}</p>
      <button onClick={() => setAge(age-1)}>-</button>
      <button onClick={() => setAge(age+1)}>+</button>
    </div>
  )
}


export default StatesIntro;
