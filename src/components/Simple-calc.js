import { useState } from "react";

const SimpleCalc = () => {
  const [value1, setValue1] = useState(0);
  const [value2, setValue2] = useState(0);
  const [result, setResult] = useState(0);

  function getRes(event) {
    event.target.innerHTML==='Sum' ? setResult(+value1 + +value2) : setResult(value1*value2);
  }
  return (
    <>
      <input value={value1} onChange={(event) => setValue1(event.target.value)}/>
      <input value={value2} onChange={(event) => setValue2(event.target.value)}/>
      <button onClick={getRes}>Sum</button>
      <button onClick={getRes}>Multiply</button>
      <p>Result {result}</p>
    </>
  )
}

export default SimpleCalc;