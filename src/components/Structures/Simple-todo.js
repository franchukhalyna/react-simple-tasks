import { useState } from "react";

const Todo = () => {
  const list = ['a', 'b', 'c', 'd', 'e'];
  const [listValue, setListValue] = useState(list);
  const removeListItem = (index) => {
    setListValue([...listValue.slice(0,index), ...listValue.slice(index+1)]);
  }
  const newList = listValue.map((el, index) => (
  <div className="listRm" key={index}>
    <p key={index}>{el}</p>
    <button onClick={() => removeListItem(index)}>Remove</button>
   </div>
  ));

  const [inputValue, setInputValue] = useState('');


  const addListValue = (event) => {
    setListValue([...listValue, event.target.value]);
    setInputValue('');
  }

  return (
    <>
      <h1>TO DO</h1>
      <div>{newList}</div>
      <input value={inputValue} onChange={(event)=>setInputValue(event.target.value)} onBlur={addListValue} onKeyDown={(event) => event.code==='Enter' && addListValue(event)}/>
    </>
  )
}

export default Todo;