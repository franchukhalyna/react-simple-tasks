// Work with checkboxes, select and radiobuttons
// 21 - When user click on button Greeting we show message: Hello when checkbox choosed or bye when checkbox unchoosed.
// 22 In text show option what user chooed using select form
// 23 In text show what radio button was choosed

import { useState } from 'react';

const CheckboxRadioSelect = () => {
  
  //21
  const [checked, setChecked] = useState(true);
  const [res, setRes] = useState('');

  function greet() {
    checked ? setRes('Hello') : setRes('Bye');
  }


  //22
  const [selectValue, setSelectValue] = useState('');
  const selectForm = (
    <select value={selectValue} onChange={handleSelectChange}>
      <option>Kiev</option>
      <option>London</option>
      <option>Paris</option>
      <option>Berlin</option>
    </select>);

  function handleSelectChange(event) {
    setSelectValue(event.target.value);
  }

  const cities = ['Kiev', 'London', 'Paris', 'Berlin'];
  const options = cities.map(el => <option>{el}</option>)
  const [selectValueArr, setSelectValueArr] = useState('');
  const selectFormArr = (
    <select value={selectValueArr} onChange={handleSelectChangeArr}>
      {options}
    </select>);

  function handleSelectChangeArr(event) {
    setSelectValueArr(event.target.value);
  }

  //23
  const radioValues = ['1','2','3'];
  const [valueRadio, setValueRadio] = useState('');
  const radioBtnsForm = <div>{radioValues.map(el => 
      <input name='radio' type='radio' value={el} 
      checked={valueRadio===el ? true : false} onChange={handleChangeRadio} />)}
    </div>

  function handleChangeRadio(event) {
    setValueRadio(event.target.value);
  }

  const [value, setValue] = useState(true);
	
	function changeHandler(event) {
		setValue(event.target.value);
	}

  return (
    <>
      {/* 21 */}
      <input type='checkbox' checked={checked} onChange={() => setChecked(!checked)}/>
      <button onClick={greet}>Greeting</button>
      <p>{res}</p>
      <br/>

      {/* 22 */}
      {selectForm}
      <p>Selected value: {selectValue}</p>

      {selectFormArr}
      <p>Selected value: {selectValueArr}</p>  
      <br/>

      {/* 23 */}

      {radioBtnsForm}
      <p>Choosed radio btn: {valueRadio}</p>
    </>
  )
}

export default CheckboxRadioSelect;