import { useState } from "react";

const BirthYear = () => {
  const [year, setYear] = useState(0);

  const handleChange = (event) => {
    setYear(event.target.value);
  }

  const birthDate = (year) => {
    return year===0 ? '' : 2022-year;
  }

  return (
    <>
      <p>How old are you?</p>
      <input value={year} onChange={handleChange} onClick={(event) => event.target.value=''}/>
      <p>Your birthday year is: {(birthDate(year))}</p>
    </>
  )
}

export default BirthYear;