import { useState } from "react";

const CountSymbols = () => {
  const [text1, setText1] = useState('');
  function handleChange1(event) {
    setText1(event.target.value)
  }

  function countSymbols(num) {
    return num.toString().split('').length;
  }
  return (
    <>
      <input value={text1} onChange={handleChange1}/>
      {/* <p>{text1}</p> */}
      <p>Number of characters: {countSymbols(text1)}</p>
    </>
  )
}


export default CountSymbols;